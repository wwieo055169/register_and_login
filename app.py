from flask import Flask,render_template,request,session,logging,url_for,redirect,flash
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session,sessionmaker
from flask_mail import Mail,Message
from itsdangerous import URLSafeTimedSerializer
import datetime

from passlib.hash import sha256_crypt
engine = create_engine("mysql+pymysql://root:1236987450qQ@localhost/register")
                        #(mysql+pymysql://username:password@localhost/databasename)
db = scoped_session(sessionmaker(bind= engine))
app = Flask(__name__)


@app.route("/")
def home():
    return render_template("home.html")

#register form

@app.route("/register",methods=["GET","POST"])
def register():

    if request.method == "POST":
        name = request.form.get("name")
        username = request.form.get("username")
        password = request.form.get("password")
        confirm = request.form.get("confirm")
        secure_password = sha256_crypt.encrypt(str(password))


        if password == confirm:
            #check database' all username
            cur= db.execute("SELECT COUNT(*) FROM users where username='%s';" % username)
            num = cur.fetchall()
            num = int(num[0][0])
            if num>=1:
                flash("somebody has already registered this username,try another one!","danger")
                return render_template("register.html")
            else:
                db.execute("INSERT INTO users(name, username, password) VALUES(:name,:username,:password)",
                                          {"name":name,"username":username,"password":secure_password})
                db.commit()
                flash("register success and can login now","success")
                return redirect(url_for('login'))
        else:
            flash("password does not match","danger")
            return render_template("register.html")
            
    return render_template("register.html")

#login form

@app.route("/login",methods=["GET","POST"])
def login():
    name = "xxx"
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")     
        usernamedata = db.execute("SELECT username FROM users WHERE username=:username",{"username":username}).fetchone()      
        passwordata = db.execute("SELECT password FROM users WHERE username=:username",{"username":username}).fetchone()
        if usernamedata is None:
            flash("No username","danger")
            return render_template("login.html")
        else:
            for passwor_data in passwordata:
                if sha256_crypt.verify(password,passwor_data):
                    session["log"] = True
                    namedata = db.execute("SELECT name FROM users WHERE username=:username",{"username":username}).fetchone()
                    usernamedata = db.execute("SELECT  username FROM users WHERE username=:username",{"username":username}).fetchone()
                    name = namedata
                    username = usernamedata
                    messagePost(username)
                    flash("You are now login","success")
                    return render_template('home.html',name = str(name))
                else:
                    flash("incorrect password","danger")
                    return render_template("login.html")

    return render_template("login.html")

#messagePost form

@app.route("/messagePost",methods=["GET","POST"])
def messagePost():
    posttext = ""
    if request.method == "POST":
        # posttext = post + date   
        timetxt = str(datetime.datetime.now().strftime('%Y-%m-%d-%H:%m'))
        posttext = request.form.get("posttext") +' / '+ timetxt

    return render_template("messagePost.html",finishtxt = posttext)

#logout form
@app.route("/logout")
def logout():
    session.clear()
    flash("you are now loggout",'success')
    return redirect(url_for('login'))

#Userset form
@app.route("/usersetting")
def usersetting():
    return render_template("username.html")

if __name__=="__main__":
    app.secret_key="123456789dialywebcoding"
    app.run(debug=True)